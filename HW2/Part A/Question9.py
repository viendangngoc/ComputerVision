# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 10:41:26 2018

@author: ngocviendang
"""  

import numpy as np
# generate a 2D array 10 x 8 of float values on interval [-10, 10]
a = np.random.uniform(-10, 10, (10, 8)) 
# get a float number from user
b = float(input("Enter a float number: ")) 
c = np.argmin(np.abs(a - b))
first_closest_value = np.ravel(a)[c]
print("The first closest value of the float number entered: " + str(first_closest_value)) 
a1 = np.delete(np.ravel(a),c)
c1 = np.argmin(np.abs(a1 - b))
second_closest_value = a1[c1]
print("The second closest value of the float number entered: " + str(second_closest_value))
a2 = np.delete(a1, c1)
c2 = np.argmin(np.abs(a2 - b))
third_closest_value = a2[c2]
print("The third closest value of the float number entered: " + str(third_closest_value))