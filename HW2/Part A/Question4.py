# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 10:30:42 2018

@author: ngocviendang
"""

import random
float_list = [] # this is the list of 100 float numbers generated
for i in range(100):
    while True:
        x = random.uniform(0,10)
        if x == 0:
            continue
        else:
            break
    float_list.append((x**2)/(4*x))
print(float_list)
print(len(float_list))