# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 22:13:34 2018

@author: ngocviendang
"""
# calculate the number of days between two dates given by users

from datetime import datetime
day_format = '%d/%m/%Y'

for i in range(2):
    if i == 0:
        date1_str = input("Enter a date (dd/MM/YYYY): ")
    else:
        date2_str = input("Enter another date (dd/MM/YYYY): ")
date1 = datetime.strptime(date1_str, day_format) # Convert String to DateTime
date2 = datetime.strptime(date2_str, day_format) # Convert String to DateTime
day_delta = date2 - date1
print("The number of days between 2 dates: " + str(day_delta.days))
