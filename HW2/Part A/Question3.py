# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 07:15:24 2018

@author: ngocviendang
"""
import random
import matplotlib.pyplot as plt
import statistics as stat
# get an interger number from users
N = int(input('Please enter an integer number less than 11: '))
# create a random list of N integers on interval [0,10] 
random_list = random.sample(range(10), N)
# plot a histogram of the list
plt.hist(random_list)
plt.title("The Histogram")
plt.xlabel("Value")
plt.ylabel("Frequency")
# calculate the statistics of the list
print("Min value element: " + str(min(random_list)))
print("Max value element: " + str(max(random_list)))
print("Mean of the list: " + str(stat.mean(random_list)))
print("Variance of the list: " + str(stat.variance(random_list)))
print("Standard deviation of the list: " + str(stat.stdev(random_list)))
# remove duplicates from the list
def dedupe(x):
    dedupe_list = []
    for i in x:
        if i not in dedupe_list:
            dedupe_list.append(i)
    return dedupe_list
print("The list after removing duplicates: " + str(dedupe(random_list)))
