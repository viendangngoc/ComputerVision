# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 22:36:42 2018

@author: ngocviendang
"""
import numpy as np
N = int(input('Enter a number: '))
a = np.random.uniform(0, 10, (N, N, N))
print("The 3D matrix: ")
print(a)
print("The min of the array by the deep of the matrix: ") 
print(a.min(axis = 0))
print("The max of the array by the deep of the matrix: ")
print(a.max(axis = 0))
print("The sum of the array by the deep of the matrix: ")
print(a.sum(axis = 0))
print("The min of the array by the height of the matrix: ") 
print(a.min(axis = 1))
print("The max of the array by the height of the matrix: ")
print(a.max(axis = 1))
print("The sum of the array by the height of the matrix: ")
print(a.sum(axis = 1))
print("The min of the array by the width of the matrix: ") 
print(a.min(axis = 2))
print("The max of the array by the width of the matrix: ")
print(a.max(axis = 2))
print("The sum of the array by the width of the matrix: ")
print(a.sum(axis = 2))
