# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 11:25:06 2018

@author: ngocviendang
"""
# calculate the sum of the elements of a matrix (lists in the list )
def matrix_sum(x):
    row_sum = []
    for i in range(len(x)):
        sum_i = sum(x[i])
        row_sum.append(sum_i)
    return sum(row_sum)
M = [[1,2,3,4,5], [3,4,2,5,6], [1,6,3,2,5]]
print("The sum of the elements of this matrix:  " + str(matrix_sum(M)))