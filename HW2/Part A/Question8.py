# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 00:35:10 2018

@author: ngocviendang
"""
import numpy as np
M = int(input("M: "))
N = int(input("N: "))
K = int(input("K: "))
a = np.random.uniform(-100, 100, (M, N, K))
print("The 3D matrix: ")
print(a)
print("The min of the array by the deep of the matrix: ") 
print(a.min(axis = 0))
print("The max of the array by the deep of the matrix: ")
print(a.max(axis = 0))
print("The sum of the array by the deep of the matrix: ")
print(a.sum(axis = 0))
print("The min of the array by the height of the matrix: ") 
print(a.min(axis = 1))
print("The max of the array by the height of the matrix: ")
print(a.max(axis = 1))
print("The sum of the array by the height of the matrix: ")
print(a.sum(axis = 1))
print("The min of the array by the width of the matrix: ") 
print(a.min(axis = 2))
print("The max of the array by the width of the matrix: ")
print(a.max(axis = 2))
print("The sum of the array by the width of the matrix: ")
print(a.sum(axis = 2))
