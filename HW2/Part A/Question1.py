# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 01:25:07 2018

@author: ngocviendang
"""
# get a string of 6 numbers from the user and generate this string two lists,
# the first list consisting the even numbers and the second one consisting the odd numbers. 
strings = ""
even_numbers = []
odd_numbers = []
print("Please enter 6 times")
for i in range(6):
    while True:
        try:
            string = input("Please enter a number: ")
            x = int(string)
            if x % 2 == 0: 
                even_numbers.append(x)
            else:
                odd_numbers.append(x)
         
            strings += string + ","
            break
        except ValueError:
            print("Oops!  That was no valid number.  Try again...")
string_input = strings[:-1]

print("The input string is: " + string_input)
print("The list of even numbers: " + str(even_numbers))
print("The list of odd numbers: " + str(odd_numbers))