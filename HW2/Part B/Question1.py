# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 11:37:18 2018

@author: ngocviendang
"""
import numpy as np
data = np.genfromtxt("Iris.txt", delimiter=",", dtype=None)
data1 = np.empty([150, 5])
a = 0
b = 0
c = 0
for i in range(len(data)):
    if data[i][4] == b'Iris-setosa':
        a += 1
        data[i][4] = 1
        data1[i] = list(data[i])
    elif data[i][4] == b'Iris-versicolor':
        b += 1
        data[i][4] = 2
        data1[i] = list(data[i])
    else:
        c += 1
        data[i][4] = 3
        data1[i] = list(data[i])
data_setosa = data1[0:a][:,0:2].T
data_versicolor = data1[a:a+b][:,0:2].T
data_virginica = data1[a+b:len(data)][:,0:2].T
print("The covariance matrix of sepal length and sepal width for setosa: ")
print(np.cov(data_setosa))
print("The covariance matrix of sepal length and sepal width for versicolor: ")
print(np.cov(data_versicolor))
print("The covariance matrix of sepal length and sepal width for virginica: ")
print(np.cov(data_virginica))
