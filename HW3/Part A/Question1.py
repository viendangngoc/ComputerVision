# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 01:53:56 2018

@author: ngocviendang
"""

import cv2
# Load the image file and display
img = cv2.imread("image.png") 
cv2.imshow("image",img) 

# Convert the color image to a gray image, save to a file
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imwrite("gray_image.png", gray)
cv2.imshow("Gray Image", gray)

# Reload the gray image file and display
gray_image = cv2.imread("gray_image.png")
cv2.imshow("gray_image",gray_image) 
cv2.waitKey(0)
cv2.destroyAllWindows()