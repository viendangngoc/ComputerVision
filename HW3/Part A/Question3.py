# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 07:31:31 2018

@author: ngocviendang
"""
import cv2
# Load the image file 
color_img = cv2.imread("image.png") 
# Gaussian blur
new_img_1 = cv2.GaussianBlur(color_img,(5,5),1)
cv2.imshow("gaussian_blur_1", new_img_1)
new_img_2 = cv2.GaussianBlur(color_img,(9,9),2)
cv2.imshow("gaussian_blur_2", new_img_2)
new_img_3 = cv2.GaussianBlur(color_img,(15,15),3)
cv2.imshow("gaussian_blur_3", new_img_3)
cv2.waitKey(0)
cv2.destroyAllWindows()

## Explain the differences
# The Gaussian filter is used to "blur" images and remove detail and noise
# The degree of smoothing is determined by sigma. Larger sigma, of course, require larger kernel sizes
# in order to accurately represented
# So as you can see the three images created above, 
# the more sigma and kernel size, the more "blur" image created
