# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 01:54:33 2018

@author: ngocviendang
"""
import cv2
## the color image file 
# Load the image file 
color_img = cv2.imread("image.png") 
# Resize the image to the size of 256 (pixels) x 256 (pixels)
new_color_img = cv2.resize(color_img, (256, 256))
#Display the image & Save to a file
cv2.imwrite("resize_color_img.png", new_color_img)
cv2.imshow("resize_color_img", new_color_img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

## the gray image 
# Reload the image file
gray_img = cv2.imread("gray_image.png")
# Resize the gray image to the  size of 256 (pixels) x 256 (pixels)
new_gray_img = cv2.resize(gray_img, (256, 256))
# Display the image and save to a file
cv2.imwrite("resize_gray_img.png", new_gray_img)
cv2.imshow("resize_gray_img", new_gray_img)
cv2.waitKey(0)
cv2.destroyAllWindows()