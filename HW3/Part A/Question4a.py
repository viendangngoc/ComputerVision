# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 10:36:25 2018

@author: ngocviendang
"""
import cv2
# Reload the image file
gray_img = cv2.imread("gray_image.png")
# Apply Canny Edge detector
edges = cv2.Canny(gray_img, 100, 200)
cv2.imshow("canndy_edges", edges)
cv2.waitKey(0)
cv2.destroyAllWindows()