# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 10:34:54 2018

@author: ngocviendang
"""
import cv2
def nothing(x):
    pass

# Load the image
img =cv2.imread("familychaos.jpg",0)

# Create trackbar for switch - ON/OFF function
cv2.namedWindow('canny')
switch = '0: OFF \n1: ON'
cv2.createTrackbar(switch,'canny',0,1,nothing)

# Create trackbar for threshold
cv2.createTrackbar('lower','canny', 0, 255, nothing)
cv2.createTrackbar('upper','canny', 0, 255, nothing)

# Canny edge detector
while True:
    # Get current positions of 3 trackbars
    lower = cv2.getTrackbarPos('lower','canny')
    upper = cv2.getTrackbarPos('upper','canny')
    s = cv2.getTrackbarPos(switch,'canny')
    # Canny edge detector
    if s == 0:
        edges = img
    else:
        edges = cv2.Canny(img, lower, upper)
    # Display images
    cv2.imshow('canny', edges)
    k = cv2.waitKey(1) & 0xFF
    if k == 27: # hit Esc to quit
        break
cv2.destroyAllWindows()