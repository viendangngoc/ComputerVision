# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 06:36:26 2018

@author: ngocviendang
"""
# Display the current date and time
import datetime
now = datetime.datetime.now()
print("Current date and time: " )
print(now.strftime("%Y-%m-%d %H-%M:%S")) 
