# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 06:42:42 2018

@author: ngocviendang
"""
def sum(x, y):
    '''
    Calculate the sum of two given numbers. 
    However, if the sum is between 15 to 20 it will return 20
    '''
    sum = x + y
    if sum in range(15,20):
        return 20
    else:
        return sum
print(sum(10, 6))   # The sum is 16, between 15 to 20 so it will return 20
print(sum(10, 2))   # The sum is 12, not between 15 to 20 so it will return 12
print(sum(10, 12))  # The sum is 22, not between 15 to 20 so it will return 22
